#include "RoboCatPCH.h"

Bar::Bar()
{
	SetColor(Vector3(0.0f, 1.0f, 0.0f));
	
}

vector<Bar*> Bar::mBars;

uint32_t Bar::Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const
{
	uint32_t writtenState = 0;

	if (inDirtyState & EHBRS_Pose)
	{
		inOutputStream.Write((bool)true);

		Vector3 location = GetLocation();
		inOutputStream.Write(location.mX);
		inOutputStream.Write(location.mY);

		inOutputStream.Write(GetRotation());


		writtenState |= EHBRS_Pose;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}

	if (inDirtyState & EHBRS_Scale)
	{
		inOutputStream.Write((bool)true);

		inOutputStream.Write(GetScale());


		writtenState |= EHBRS_Scale;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}

	if (inDirtyState & EHBRS_Colour)
	{
		inOutputStream.Write((bool)true);

		inOutputStream.Write(GetColor());

		writtenState |= EHBRS_Colour;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}

	return writtenState;
}

void Bar::Read(InputMemoryBitStream& inInputStream)
{
	bool stateBit;

	Team teamType;
	int health;

	inInputStream.Read(stateBit);
	if (stateBit)
	{
		Vector3 location;
		inInputStream.Read(location.mX);
		inInputStream.Read(location.mY);
		SetLocation(location);

		float rotation;
		inInputStream.Read(rotation);
		SetRotation(rotation);

		//readState |= EHBRS_Pose;
	}

	inInputStream.Read(stateBit);
	if (stateBit)
	{
		int scale;
		inInputStream.Read(scale);
		SetScale(scale);
		//readState |= EHBRS_Scale;
	}

	inInputStream.Read(stateBit);
	if (stateBit)
	{
		Vector3 colour;
		inInputStream.Read(colour);
		SetColor(colour);

		//readState |= EHBRS_Colour;
	}


}
