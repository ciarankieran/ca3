#include <RoboCatPCH.h>


Base::Base() : GameObject()
{
	SetScale(GetScale() * .8f);
	SetCollisionRadius(1.2f);
}

vector<Base*> Base::mBases;

//bool Base::HandleCollisionWithCat(RoboCat* inCat)
//{
//	(void)inCat;
//	return false;
//}

uint32_t Base::Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const
{
	uint32_t writtenState = 0;

	if (inDirtyState & EBRS_Pose)
	{
		inOutputStream.Write((bool)true);

		Vector3 location = GetLocation();
		inOutputStream.Write(location.mX);
		inOutputStream.Write(location.mY);

		inOutputStream.Write(GetRotation());

		writtenState |= EBRS_Pose;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}

	if (inDirtyState & EBRS_Team)
	{
		inOutputStream.Write((bool)true);

		inOutputStream.Write(static_cast<int>(GetTeam()));

		writtenState |= EBRS_Team;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}

	if (inDirtyState & EBRS_Health)
	{
		inOutputStream.Write((bool)true);

		inOutputStream.Write(mBaseHealth, 4);

		writtenState |= EBRS_Health;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}

	return writtenState;
}



