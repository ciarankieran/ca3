#include "RoboCatPCH.h"


Obstacle::Obstacle()
{
	SetScale(GetScale() * .6f);
	SetCollisionRadius(0.6f);
}

vector<Obstacle*> Obstacle::mObstacles;

//bool Obstacle::HandleCollisionWithCat(RoboCat* inCat)
//{
//	(void)inCat;
//	return false;
//}

uint32_t Obstacle::Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const
{
	uint32_t writtenState = 0;

	if (inDirtyState & EORS_Pose)
	{
		inOutputStream.Write((bool)true);

		Vector3 location = GetLocation();
		inOutputStream.Write(location.mX);
		inOutputStream.Write(location.mY);

		inOutputStream.Write(GetRotation());

		writtenState |= EORS_Pose;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}

	return writtenState;
}

void Obstacle::Read(InputMemoryBitStream& inInputStream)
{
	bool stateBit;

	inInputStream.Read(stateBit);
	if (stateBit)
	{
		Vector3 location;
		inInputStream.Read(location.mX);
		inInputStream.Read(location.mY);
		SetLocation(location);

		float rotation;
		inInputStream.Read(rotation);
		SetRotation(rotation);
	}
}