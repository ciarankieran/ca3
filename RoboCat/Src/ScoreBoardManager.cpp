#include "RoboCatPCH.h"

std::unique_ptr< ScoreBoardManager >	ScoreBoardManager::sInstance;


void ScoreBoardManager::StaticInit()
{
	sInstance.reset( new ScoreBoardManager() );
}

ScoreBoardManager::ScoreBoardManager()
{
	mDefaultColors.push_back( Colors::LightYellow );
	mDefaultColors.push_back( Colors::LightBlue );
	mDefaultColors.push_back( Colors::LightPink );
	mDefaultColors.push_back( Colors::LightGreen );
}

ScoreBoardManager::Entry::Entry( uint32_t inPlayerId, const string& inPlayerName, const Team inTeam, int inPreviousWins ) :
mPlayerId( inPlayerId ),
mPlayerName( inPlayerName ),
mTeam( inTeam ),
mPreviousWins(inPreviousWins)
{
	SetScore( 0 );
}

void ScoreBoardManager::Entry::SetScore( int32_t inScore )
{
	mKills = inScore;
	string paddedString = PadString(mPlayerName, 20);
	paddedString += " " + PadString(std::to_string(mKills), 7);
	paddedString += " " + PadString(std::to_string(mPreviousWins), 7);
	mFormattedNameScore = paddedString;
}

void ScoreBoardManager::Entry::IncrementWins()
{
	mPreviousWins++;
	string paddedString = PadString(mPlayerName, 20);
	paddedString += PadString(std::to_string(mKills), 7);
	paddedString += PadString(std::to_string(mPreviousWins), 7);
	mFormattedNameScore = paddedString;
}


ScoreBoardManager::Entry* ScoreBoardManager::GetEntry( uint32_t inPlayerId )
{
	for ( Entry &entry: mEntries )
	{
		if( entry.GetPlayerId() == inPlayerId )
		{
			return &entry;
		}
	}

	return nullptr;
}

bool ScoreBoardManager::RemoveEntry( uint32_t inPlayerId )
{
	for( auto eIt = mEntries.begin(), endIt = mEntries.end(); eIt != endIt; ++eIt )
	{
		if( ( *eIt ).GetPlayerId() == inPlayerId )
		{
			mEntries.erase( eIt );
			return true;
		}
	}

	return false;
}

void ScoreBoardManager::AddEntry( uint32_t inPlayerId, const string& inPlayerName, Team inPlayerTeam, int inPlayerWins )
{
	//if this player id exists already, remove it first- it would be crazy to have two of the same id
	RemoveEntry( inPlayerId );
	
	mEntries.emplace_back( inPlayerId, inPlayerName, inPlayerTeam, inPlayerWins);

	std::sort(mEntries.begin(), mEntries.end(), [](const Entry& lhs, const Entry& rhs)
	{
		return (lhs.GetTeam() < rhs.GetTeam()) && (lhs.GetScore() >= rhs.GetScore());
	});
}

void ScoreBoardManager::IncScore( uint32_t inPlayerId, int inAmount )
{
	Entry* entry = GetEntry( inPlayerId );
	if( entry )
	{
		entry->SetScore( entry->GetScore() + inAmount );
	}
}

void ScoreBoardManager::IncWins(Team winningTeam)
{
	for each (Entry entry in mEntries)
	{
		if (entry.GetTeam() == winningTeam)
		{
			entry.IncrementWins();
		}
	}
}



bool ScoreBoardManager::Write( OutputMemoryBitStream& inOutputStream ) const
{
	int entryCount = mEntries.size();
	
	//we don't know our player names, so it's hard to check for remaining space in the packet...
	//not really a concern now though
	inOutputStream.Write( entryCount );
	for( const Entry& entry: mEntries )
	{
		entry.Write( inOutputStream );
	}
	
	return true;
}



bool ScoreBoardManager::Read( InputMemoryBitStream& inInputStream )
{
	int entryCount;
	inInputStream.Read( entryCount );
	//just replace everything that's here, it don't matter...
	mEntries.resize( entryCount );
	for( Entry& entry: mEntries )
	{
		entry.Read( inInputStream );
	}

	return true;
}


bool ScoreBoardManager::Entry::Write( OutputMemoryBitStream& inOutputStream ) const
{
	bool didSucceed = true;

	inOutputStream.Write( static_cast<int>(mTeam));
	inOutputStream.Write( mPlayerId );
	inOutputStream.Write( mPlayerName );
	inOutputStream.Write(mPreviousWins);
	inOutputStream.Write( mKills );


	return didSucceed;
}

bool ScoreBoardManager::Entry::Read( InputMemoryBitStream& inInputStream )
{
	bool didSucceed = true;

	inInputStream.Read( static_cast<Team>(mTeam));
	inInputStream.Read( mPlayerId );
	inInputStream.Read( mPlayerName );
	inInputStream.Read(mPreviousWins);

	int score;
	inInputStream.Read( score );
	
	if( didSucceed )
	{
		SetScore( score );
	}

	

	return didSucceed;
}


std::string ScoreBoardManager::Entry::PadString(std::string toPad, int length)
{
	while (toPad.length() < length)
	{
		toPad += ' ';
	}

	while (toPad.length() > length)
	{
		toPad.erase(toPad.end());
	}

	return toPad;
}

