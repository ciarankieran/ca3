/*
Worked on by:		Kieran
Purpose:			Obstacles on map for player and bullets
Notes:				
*/
class Obstacle : public GameObject
{
public:
	CLASS_IDENTIFICATION('OBST', GameObject)

		enum EObstacleReplicationState
	{
		EORS_Pose = 1 << 0,


		EORS_AllState = EORS_Pose
	};

	static	GameObject*	StaticCreate() { return new Obstacle(); }

	virtual uint32_t	GetAllStateMask()	const override { return EORS_AllState; }

	virtual uint32_t	Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const override;
	virtual void		Read(InputMemoryBitStream& inInputStream) override;

	//virtual bool HandleCollisionWithCat(RoboCat* inCat) override;
	virtual Obstacle*		GetAsObstacle() { return this; }


protected:
	Obstacle();

public:
	static vector<Obstacle*>						mObstacles;
};

typedef shared_ptr< Obstacle >	ObstaclePtr;