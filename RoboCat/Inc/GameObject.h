/*
Modifications by:	Ciaran and Kieran
Purpose:			Base class for all game objects
Notes:				We added functions such as GetAsBase(), SetTeam(),	GetTurretForwardVector() etc
*/

#define CLASS_IDENTIFICATION( inCode, inClass ) \
enum { kClassId = inCode }; \
virtual uint32_t GetClassId() const { return kClassId; } \
static GameObject* CreateInstance() { return static_cast< GameObject* >( new inClass() ); } \

enum Team
{
	Liberator,
	Resistance,
	Any = Liberator | Resistance
};
class GameObject
{
public:

	CLASS_IDENTIFICATION( 'GOBJ', GameObject )

	GameObject();
	virtual ~GameObject() {}

	virtual	RoboCat*	GetAsCat()	{ return nullptr; }
	virtual Base*		GetAsBase() { return nullptr; }
	virtual Bar*		GetAsBar() { return nullptr; }
	virtual Obstacle*	GetAsObstacle() { return nullptr; }

	virtual uint32_t GetAllStateMask()	const { return 0; }

	//return whether to keep processing collision
	virtual bool	HandleCollisionWithCat( RoboCat* inCat ) { ( void ) inCat; return true; }

	virtual void	Update();

	virtual void	HandleDying() {}

	void	SetIndexInWorld( int inIndex )						{ mIndexInWorld = inIndex; }
	int		GetIndexInWorld()				const				{ return mIndexInWorld; }

	void	SetRotation( float inRotation );
	float	GetRotation()					const				{ return mRotation; }

	virtual float GetTurretRotation() const { return 0; };

	void	SetScale( float inScale )							{ mScale = inScale; }
	float	GetScale()						const				{ return mScale; }


	const Vector3&		GetLocation()				const		{ return mLocation; }
	void		SetLocation( const Vector3& inLocation )		{ mLocation = inLocation; }

	float		GetCollisionRadius()		const				{ return mCollisionRadius; }
	void		SetCollisionRadius( float inRadius )			{ mCollisionRadius = inRadius; }

	Vector3	GetForwardVector() const;
	virtual Vector3	GetTurretForwardVector() const { return Vector3(0, 0, 0); };


	void SetColor( const Vector3& inColor )	{ mColor = inColor; }
	const Vector3&		GetColor()					const		{ return mColor; }

	bool		DoesWantToDie()				const				{ return mDoesWantToDie; }
	void		SetDoesWantToDie( bool inWants )				{ mDoesWantToDie = inWants; }

	int			GetNetworkId()				const				{ return mNetworkId; }
	void		SetNetworkId( int inNetworkId );

	virtual Vector3 GetSpriteComponentBounds() const { return Vector3(0, 0, 0); }; 

	//const bool			GetTeam()					const				{ return mIsResistance; }

	void SetTeam(Team teamChoice) { mTeam = teamChoice; }
	const Team	GetTeam() const	{ return mTeam; }

	virtual void		SetBaseHealth(int inHealth) {  }
	virtual const int&		GetBaseHealth()						const { return 1; }

	virtual uint32_t	Write( OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState ) const	{ (void)inOutputStream; (void)inDirtyState; return 0; }
	virtual void		Read( InputMemoryBitStream& inInputStream )									{ (void)inInputStream; }

	static Vector3 GetTeamColor(Team team);

private:


	Vector3											mLocation;
	Vector3											mColor;
	Team											mTeam;
	
	
	float											mCollisionRadius;

	float											mRotation;
	float											mScale;
	int												mIndexInWorld;

	bool											mDoesWantToDie;

	int												mNetworkId;
};

typedef shared_ptr< GameObject >	GameObjectPtr;