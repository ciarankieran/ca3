class ScoreBoardManager
{
public:

	static void StaticInit();
	static std::unique_ptr< ScoreBoardManager >	sInstance;

	class Entry
	{
	public:
		Entry() {};

		Entry( uint32_t inPlayerID, const string& inPlayerName, const Team inTeam, int inPreviousWins);

		const Team&	GetTeam()		const	{ return mTeam; }
		uint32_t		GetPlayerId()	const	{ return mPlayerId; }
		const string&	GetPlayerName()	const	{ return mPlayerName; }
		const string&	GetFormattedNameScore() const	{ return mFormattedNameScore; }
		int				GetScore()		const	{ return mKills; }

		void			SetScore( int inScore );
		void			IncrementWins();

		int				GetPlayerWins() { return mPreviousWins; }

		bool			Write( OutputMemoryBitStream& inOutputStream ) const;
		bool			Read( InputMemoryBitStream& inInputStream );
		static uint32_t	GetSerializedSize();
		

	private:
		Team			mTeam;
		
		uint32_t		mPlayerId;
		string			mPlayerName;
		
		int				mKills;
		int				mPreviousWins;

		string			mFormattedNameScore;
		std::string		PadString(std::string toPad, int length);
	};

	Entry*	GetEntry( uint32_t inPlayerId );
	bool	RemoveEntry( uint32_t inPlayerId );
	void	AddEntry( uint32_t inPlayerId, const string& inPlayerName, Team inTeam, int inPreviousWins );
	void	IncScore( uint32_t inPlayerId, int inAmount );
	void	IncWins(Team winningTeam);

	bool	Write( OutputMemoryBitStream& inOutputStream ) const;
	bool	Read( InputMemoryBitStream& inInputStream );

	const vector< Entry >&	GetEntries()	const	{ return mEntries; }

private:

	ScoreBoardManager();

	vector< Entry >	mEntries;

	vector< Vector3 >	mDefaultColors;
	

};