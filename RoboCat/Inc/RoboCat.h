/*
Worked on by:		Ciaran
Purpose:			Tank class controlled by player
Notes:
*/
class RoboCat : public GameObject
{
public:
	CLASS_IDENTIFICATION( 'RCAT', GameObject )

	enum ECatReplicationState
	{
		ECRS_Turret_Rotation = 1 << 0,
		ECRS_Pose = 1 << 1,
		ECRS_Team = 1 << 2,
		ECRS_PlayerId = 1 << 3,
		ECRS_Health = 1 << 4,

		ECRS_AllState = ECRS_Turret_Rotation | ECRS_Pose | ECRS_Team | ECRS_PlayerId | ECRS_Health
	};

	static	GameObject*	StaticCreate()			{ return new RoboCat(); }

	virtual uint32_t GetAllStateMask()	const override	{ return ECRS_AllState; }

	virtual	RoboCat*	GetAsCat()	{ return this; }

	virtual void Update()	override;

	void ProcessInput( float inDeltaTime, const InputState& inInputState );
	void SimulateMovement( float inDeltaTime );

	void ProcessCollisions();
	void ProcessCollisionsWithScreenWalls();

	void		SetPlayerId( uint32_t inPlayerId )			{ mPlayerId = inPlayerId; }
	uint32_t	GetPlayerId()						const 	{ return mPlayerId; }

	void			SetVelocity( const Vector3& inVelocity )	{ mVelocity = inVelocity; }
	const Vector3&	GetVelocity()						const	{ return mVelocity; }

	void SetTurretRotation(const float newRotation)	{ mTurretRotation = newRotation; }
	virtual float GetTurretRotation() const override { return mTurretRotation; } 

	virtual Vector3		GetTurretForwardVector() const override;

	virtual uint32_t	Write( OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState ) const override;

protected:
	RoboCat();

private:


	void	AdjustVelocityByThrust( float inDeltaTime );

	Vector3				mVelocity;
	float				mTurretRotation;

	float				mMaxLinearSpeed;
	float				mMaxRotationSpeed;
	float				mMaxTurretRotationSpeed;

	//bounce fraction when hitting various things
	float				mWallRestitution;
	float				mCatRestitution;

	uint32_t			mPlayerId;
	bool				mPlayerTeam;

protected:

	///move down here for padding reasons...
	
	float				mLastMoveTimestamp;

	float				mThrustDir;
	int					mHealth;

	bool				mIsShooting;
};

typedef shared_ptr< RoboCat >	RoboCatPtr;