
/*
* the world tracks all the live game objects. Failry inefficient for now, but not that much of a problem
*/
class World
{

public:

	static void StaticInit();

	static std::unique_ptr< World >		sInstance;

	void AddGameObject( GameObjectPtr inGameObject );
	void RemoveGameObject( GameObjectPtr inGameObject );

	Vector3 GetViewportScale() { return mViewportScale; };
	void SetViewportScale(Vector3 viewport) { mViewportScale = viewport; };

	Vector3 GetMapTextureBounds() { return mMapTextureBounds; };
	void SetMapTextureBounds(Vector3 bounds) { mMapTextureBounds = bounds; };

	void Update();

	const std::vector< GameObjectPtr >&	GetGameObjects()	const	{ return mGameObjects; }

private:


	World();

	int	GetIndexOfGameObject( GameObjectPtr inGameObject );

	std::vector< GameObjectPtr >	mGameObjects;

	Vector3 mViewportScale;
	Vector3 mMapTextureBounds;
};