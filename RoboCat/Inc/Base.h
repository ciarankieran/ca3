/*
Worked on by:	Kieran
Purpose:		Team base, when destroyed other team win
Notes:			
*/
class Base : public GameObject
{
public:
	CLASS_IDENTIFICATION('BASE', GameObject)

	enum EBaseReplicationState
	{
		EBRS_Pose = 1 << 0,
		EBRS_Team = 1 << 1,
		EBRS_Health = 1 << 2,

		EBRS_AllState = EBRS_Pose | EBRS_Team | EBRS_Health 
	};

	static	GameObject*	StaticCreate() { return new Base(); }

	virtual uint32_t	GetAllStateMask()	const override { return EBRS_AllState; }

	virtual uint32_t	Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const override;
	//virtual void		Read(InputMemoryBitStream& inInputStream) override;

	//virtual bool HandleCollisionWithCat(RoboCat* inCat) override;

	virtual void		SetBaseHealth(int inHealth) override { mBaseHealth = inHealth; }
	virtual const int&		GetBaseHealth()						const override { return mBaseHealth; }
	
	virtual Base*		GetAsBase() { return this; }
	
	//Bar* mBarPtr;

protected:
	Base();
	int												mBaseHealth, mBaseStartingHealth;

public:
	static vector<Base*>						mBases;

	
};
typedef shared_ptr< Base >	BasePtr;
