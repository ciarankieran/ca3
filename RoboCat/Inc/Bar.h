/*
Worked on by:	Kieran
Purpose:		Health Bar under Base
Notes:			Had to be removed as causing pointer error
*/
#pragma once
class Bar : public GameObject
{
public:
	CLASS_IDENTIFICATION('HBAR', GameObject)

		enum EBarReplicationState
	{
		EHBRS_Pose = 1 << 0,
		EHBRS_Scale = 1 << 1,
		EHBRS_Colour = 1 << 2,



		EHBRS_AllState = EHBRS_Pose || EHBRS_Scale || EHBRS_Colour
	};

	static	GameObject*	StaticCreate() { return new Bar(); }

	virtual uint32_t	GetAllStateMask()	const override { return EHBRS_AllState; }

	virtual uint32_t	Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const override;
	virtual void		Read(InputMemoryBitStream& inInputStream) override;

	virtual Bar*		GetAsBar() { return this; }

protected:
	Bar();

public:
	static vector<Bar*>						mBars;
};



