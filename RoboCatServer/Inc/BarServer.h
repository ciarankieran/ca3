/*
Worked on by:		Kieran
Purpose:			Server side of Bar class
Notes:
*/
#pragma once
class BarServer : public Bar
{
public:
	static GameObjectPtr	StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn(new BarServer()); }
	void HandleDying() override;
	virtual bool		HandleCollisionWithCat(RoboCat* inCat) override;

	void TakeDamage();

	int hitCount;

protected:
	BarServer();
};
