/*
Worked on by:		Kieran
Purpose:			Server side of obstacle class
Notes:
*/
#pragma once
class ObstacleServer : Obstacle
{
public:
	static GameObjectPtr	StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn(new ObstacleServer()); }
	void HandleDying() override;
	virtual bool		HandleCollisionWithCat(RoboCat* inCat) override;
	//void TakeDamage(int inDamagingPlayerId);

protected:
	ObstacleServer();
};


