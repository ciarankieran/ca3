/*
Worked on by:		Kieran & Ciaran
Purpose:			Server side of Bullet class
Notes:				Handles bullet collision detection with Base and Obstacle
*/
class YarnServer : public Yarn
{
public:
	static GameObjectPtr	StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn( new YarnServer() ); }
	void HandleDying() override;

	virtual bool		HandleCollisionWithCat( RoboCat* inCat ) override;
	bool				HandleCollisionWithBase(Base* inBase);

	virtual void Update() override;

protected:
	YarnServer();

private:
	float mTimeToDie;

	void CheckBase(Base* inBase);
	void CheckObstacles(Obstacle* inObs);

};