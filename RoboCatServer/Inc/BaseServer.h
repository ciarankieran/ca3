/*
Worked on by:		Kieran
Purpose:			Server side of Base class
Notes:				Handles taking damage and base death
*/
class BaseServer : public  Base
{
public:
	static GameObjectPtr	StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn(new BaseServer()); }
	void HandleDying() override;
	virtual bool		HandleCollisionWithCat(RoboCat* inCat) override;
	void TakeDamage();

protected:
	BaseServer();
};

