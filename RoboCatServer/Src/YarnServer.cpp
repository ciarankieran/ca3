#include <RoboCatServerPCH.h>


YarnServer::YarnServer()
{
	//yarn lives a second...
	mTimeToDie = Timing::sInstance.GetFrameStartTime() + 2.f;
}

void YarnServer::HandleDying()
{
	NetworkManagerServer::sInstance->UnregisterGameObject( this );
}


void YarnServer::Update()
{
	Yarn::Update();

	if( Timing::sInstance.GetFrameStartTime() > mTimeToDie )
	{
		SetDoesWantToDie( true );
	}

	for each (Base* base in Base::mBases)
	{
		if (base)
		{
			CheckBase(base);
		}
	}

	for each (Obstacle* obs in Obstacle::mObstacles)
	{
		if (obs)
		{
			CheckObstacles(obs);
		}
	}
}

void YarnServer::CheckBase(Base* inBase)
{
	Vector3 baseLocation = inBase->GetLocation();
	float padding = 1;

	if ((GetLocation().mX <= baseLocation.mX + padding && GetLocation().mX >= baseLocation.mX - padding) &&
		(GetLocation().mY <= baseLocation.mY + padding && GetLocation().mY >= baseLocation.mY - padding))
	{
		SetDoesWantToDie(true);
		static_cast< BaseServer* >(inBase)->TakeDamage();
	}
}

void YarnServer::CheckObstacles(Obstacle* inObs)
{
	Vector3 obsLocation = inObs->GetLocation();
	float padding = .5f;

	if ((GetLocation().mX <= obsLocation.mX + padding && GetLocation().mX >= obsLocation.mX - padding) &&
		(GetLocation().mY <= obsLocation.mY + padding && GetLocation().mY >= obsLocation.mY - padding))
	{
		SetDoesWantToDie(true);
	}
}

bool YarnServer::HandleCollisionWithCat( RoboCat* inCat )
{
	if( inCat->GetTeam() != GetTeam() )
	{
		//kill yourself!
		SetDoesWantToDie( true );

		static_cast< RoboCatServer* >( inCat )->TakeDamage( GetPlayerId() );
	}

	return false;
}

bool YarnServer::HandleCollisionWithBase(Base* inBase)
{
	if (inBase->GetTeam() != GetTeam())
	{
		//kill yourself!
		SetDoesWantToDie(true);

		static_cast< BaseServer* >(inBase)->TakeDamage();

	}

	return false;
}


