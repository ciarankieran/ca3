#include "RoboCatServerPCH.h"


BaseServer::BaseServer() 
{
	mBaseStartingHealth = 20;
	SetBaseHealth(mBaseStartingHealth);
}


void BaseServer::HandleDying()
{
	//NetworkManagerServer::sInstance->UnregisterGameObject(this);
}

void BaseServer::TakeDamage()
{
	mBaseHealth -= 5;
	
	//float red = 1.f - (mBaseHealth / (float)mBaseStartingHealth);
	//float green = (mBaseHealth / (float)mBaseStartingHealth);

	if (mBaseHealth <= 0)
	{
		//and you want to die
		SetDoesWantToDie(true); ///Change to kill base
		//mBarPtr->SetDoesWantToDie(true);
		if (GetTeam() == Team::Liberator)
		{
			ScoreBoardManager::sInstance->IncWins(Team::Resistance);
		}
		else
		{
			ScoreBoardManager::sInstance->IncWins(Team::Liberator);
		}
	}

	//tell the world our health dropped
	NetworkManagerServer::sInstance->SetStateDirty(GetNetworkId(), EBRS_Health);
	//NetworkManagerServer::sInstance->SetStateDirty(mBarPtr->GetNetworkId(), EHBRS_Colour);
}


bool BaseServer::HandleCollisionWithCat(RoboCat* inCat)
{
	//kill yourself!
	//SetDoesWantToDie(true);

	//ScoreBoardManager::sInstance->IncScore(inCat->GetPlayerId(), 1);

	return false;
}
