#include "RoboCatServerPCH.h"


BarServer::BarServer()
{
	hitCount = 0;
}

void BarServer::HandleDying()
{
	NetworkManagerServer::sInstance->UnregisterGameObject(this);
}

bool BarServer::HandleCollisionWithCat(RoboCat* inCat)
{
	return false;
}

void BarServer::TakeDamage()
{
	if (hitCount == 0)
	{ //Hit  1 Times -- Yella 1.0f, 1.0f, 0.0f
		this->SetColor(Vector3(1.0f, 1.0f, 0.0f));
		NetworkManagerServer::sInstance->SetStateDirty(GetNetworkId(), EHBRS_Colour);
		//this->SetScale(.6f);
		//NetworkManagerServer::sInstance->SetStateDirty(GetNetworkId(), EHBRS_Scale);
		hitCount++;
	}
	else if (hitCount == 1)
	{ //Hit  2 Times -- Orange 1.0f, 0.6f, 0.0f
		this->SetColor(Vector3(1.0f, 0.6f, 0.0f));
		NetworkManagerServer::sInstance->SetStateDirty(GetNetworkId(), EHBRS_Colour);
		hitCount++;
	}
	else if (hitCount == 2)
	{ //Hit  3 Times -- Red
		this->SetColor(Vector3(1.0f, 0.0f, 0.0f));
		NetworkManagerServer::sInstance->SetStateDirty(GetNetworkId(), EHBRS_Colour);
		hitCount++;
	}
	else if (hitCount == 3)
	{ //Hit  4 Times -- Dead
		this->SetColor(Vector3(1.0f, 1.0f, 1.0f));
		NetworkManagerServer::sInstance->SetStateDirty(GetNetworkId(), EHBRS_Colour);
		this->SetDoesWantToDie(true);
	}
}