
#include <RoboCatServerPCH.h>



//uncomment this when you begin working on the server

bool Server::StaticInit()
{
	sInstance.reset( new Server() );

	return true;
}

Server::Server()
{

	GameObjectRegistry::sInstance->RegisterCreationFunction( 'RCAT', RoboCatServer::StaticCreate );
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'MOUS', MouseServer::StaticCreate );
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'YARN', YarnServer::StaticCreate );
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'BASE', BaseServer::StaticCreate );
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'OBST', ObstacleServer::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'HBAR', BarServer::StaticCreate);

	InitNetworkManager();
	
	//NetworkManagerServer::sInstance->SetDropPacketChance( 0.8f );
	//NetworkManagerServer::sInstance->SetSimulatedLatency( 0.25f );
	//NetworkManagerServer::sInstance->SetSimulatedLatency( 0.5f );
	//NetworkManagerServer::sInstance->SetSimulatedLatency( 0.1f );

}


int Server::Run()
{
	SetupWorld();

	return Engine::Run();
}

bool Server::InitNetworkManager()
{
	string portString = StringUtils::GetCommandLineArg( 1 );
	uint16_t port = stoi( portString );

	return NetworkManagerServer::StaticInit( port );
}


namespace
{
	
	void CreateRandomMice( int inMouseCount )
	{
		Vector3 mouseMin( -5.f, -3.f, 0.f );
		Vector3 mouseMax( 5.f, 3.f, 0.f );
		GameObjectPtr go;

		//make a mouse somewhere- where will these come from?
		for( int i = 0; i < inMouseCount; ++i )
		{
			go = GameObjectRegistry::sInstance->CreateGameObject( 'MOUS' );
			Vector3 mouseLocation = RoboMath::GetRandomVector( mouseMin, mouseMax );
			go->SetLocation( mouseLocation );
		}
	}

	void CreateBases(int inBaseCount)
	{
		GameObjectPtr go;
		GameObjectPtr hBar;

		for (int i = 0; i < inBaseCount; i++)
		{
			go = GameObjectRegistry::sInstance->CreateGameObject('BASE');
			go->SetLocation(Vector3(-14, 0, 0));
			go->SetTeam(Team::Liberator);
			hBar = GameObjectRegistry::sInstance->CreateGameObject('HBAR');
			hBar->SetTeam(Team::Liberator);
			hBar->SetLocation(Vector3(-14, 1.7f, 0));
			
			if (i == 1)
			{
				go->SetLocation(Vector3(14, 0, 0));
				go->SetTeam(Team::Resistance);
				hBar->SetTeam(Team::Resistance);
				hBar->SetLocation(Vector3(14, 1.7f, 0));
			}

			hBar->SetScale(.8f);
			hBar->SetColor(Vector3(0.0f, 1.0f, 0.0f));

			go->SetBaseHealth(20);

			Bar* barPtr = hBar->GetAsBar();
			Bar::mBars.push_back(barPtr);
			Base* basePtr = go->GetAsBase();
			Base::mBases.push_back(basePtr);
		}
	}

	void CreateObstacles(int inObstacleCount)
	{
		Vector3 obstacleMin(-6.f, -4.f, 0.f);
		Vector3 obstacleMax(6.f, 4, 0.f);
		GameObjectPtr go;
		for (int i = 0; i < inObstacleCount; ++i)
		{
			go = GameObjectRegistry::sInstance->CreateGameObject('OBST');
			Vector3 obstacleLocation = Vector3(-4, 0 + i/2, 0);//RoboMath::GetRandomVector(obstacleMin, obstacleMax);
			if (i % 2 != 0)
			{
				obstacleLocation = Vector3(4, 0 + i/2, 0); //RoboMath::GetRandomVector(obstacleMin, obstacleMax);
			}
			go->SetLocation(obstacleLocation);
			Obstacle* obstaclePtr = go->GetAsObstacle();
			Obstacle::mObstacles.push_back(obstaclePtr);
		}
	}

	void CreateObstacles()
	{
		GameObjectPtr go;
		go = GameObjectRegistry::sInstance->CreateGameObject('OBST');
		go->SetLocation(Vector3(-3, 0, 0));
	}
}


void Server::SetupWorld()
{
	//spawn some random mice
	//CreateRandomMice( 10 );
	
	//spawn more random mice!
	//CreateRandomMice( 10 );

	//Spawn base
	CreateBases(2);

	//Spawn obstacles
	CreateObstacles(4);
}

void Server::DoFrame()
{
	NetworkManagerServer::sInstance->ProcessIncomingPackets();

	NetworkManagerServer::sInstance->CheckForDisconnects();

	NetworkManagerServer::sInstance->RespawnCats();

	Engine::DoFrame();

	NetworkManagerServer::sInstance->SendOutgoingPackets();

}

void Server::HandleNewClient( ClientProxyPtr inClientProxy )
{
	
	int playerId = inClientProxy->GetPlayerId();
	inClientProxy->SetPlayerTeam(playerId % 2 != 0 ? Team::Liberator : Team::Resistance);

	ScoreBoardManager::sInstance->AddEntry( playerId, inClientProxy->GetName(), inClientProxy->GetPlayerTeam(), inClientProxy->GetPlayerWins());
	SpawnCatForPlayer( playerId, inClientProxy->GetPlayerTeam() );
}

void Server::SpawnCatForPlayer( int inPlayerId , Team inPlayerTeam)
{
	RoboCatPtr cat = std::static_pointer_cast< RoboCat >( GameObjectRegistry::sInstance->CreateGameObject( 'RCAT' ) );
	cat->SetTeam(inPlayerTeam);
	cat->SetPlayerId( inPlayerId );
	//gotta pick a better spawn location than this...
	cat->SetLocation(Vector3(11.f, (-1.f + static_cast< float >(inPlayerId/2)), 0.f));
	cat->SetRotation(-1.5708f);
	if (inPlayerTeam == Team::Liberator)
	{
		cat->SetLocation(Vector3(-11.f, (-1.f + static_cast< float >(inPlayerId/2)), 0.f));
		cat->SetRotation(1.5708f);
	}
	NetworkManagerServer::sInstance->SetStateDirty(inPlayerId, RoboCat::ECatReplicationState::ECRS_Team);

}

void Server::HandleLostClient( ClientProxyPtr inClientProxy )
{
	//kill client's cat
	//remove client from scoreboard
	int playerId = inClientProxy->GetPlayerId();

	ScoreBoardManager::sInstance->RemoveEntry( playerId );
	RoboCatPtr cat = GetCatForPlayer( playerId );
	if( cat )
	{
		cat->SetDoesWantToDie( true );
	}
}

RoboCatPtr Server::GetCatForPlayer( int inPlayerId )
{
	//run through the objects till we find the cat...
	//it would be nice if we kept a pointer to the cat on the clientproxy
	//but then we'd have to clean it up when the cat died, etc.
	//this will work for now until it's a perf issue
	const auto& gameObjects = World::sInstance->GetGameObjects();
	for( int i = 0, c = gameObjects.size(); i < c; ++i )
	{
		GameObjectPtr go = gameObjects[ i ];
		RoboCat* cat = go->GetAsCat();
		if( cat && cat->GetPlayerId() == inPlayerId )
		{
			return std::static_pointer_cast< RoboCat >( go );
		}
	}

	return nullptr;

}