#pragma once
/*
Worked on by:		Kieran & Ciaran
Purpose:			Background image
Notes:				Kieran made class, Ciaran increased size and implemented view bounds and camera tracking player
*/
class Map : public GameObject
{
public:
	static	GameObjectPtr	StaticCreate() { return GameObjectPtr(new Map()); }

	virtual bool HandleCollisionWithCat(RoboCat* inCat) override;

	Vector3 GetSpriteComponentBounds() const override { return mSpriteComponent->GetTextureBounds(); };

protected:
	Map();

private:
	SpriteComponentPtr	mSpriteComponent;
};

