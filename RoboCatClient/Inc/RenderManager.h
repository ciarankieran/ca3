//I take care of rendering things!

class RenderManager
{
public:

	static void StaticInit();
	static std::unique_ptr< RenderManager >	sInstance;

	void Render();
	void RenderComponents();

	//vert inefficient method of tracking scene graph...
	void AddComponent( SpriteComponent* inComponent );
	void RemoveComponent( SpriteComponent* inComponent );
	int	 GetComponentIndex( SpriteComponent* inComponent ) const;
	void SetMapBounds(Vector3 bounds) { mapBounds = bounds; };

	void UpdateTransformPositionToPosition(Vector3 position);

private:

	RenderManager();

	//this can't be only place that holds on to component- it has to live inside a GameObject in the world
	vector< SpriteComponent* >		mComponents;
	vector< SpriteComponent* >		mTurretComponents;

	SDL_Rect						mViewTransform;
	int viewportHeight, viewportWidth;
	Vector3 mapBounds;
};

