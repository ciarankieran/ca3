//I take care of rendering things!

/*
Worked on by:		Ciaran
Purpose:			Player Heads up display
Notes:
*/
#include <SDL_ttf.h>

class HUD
{
public:

	static void StaticInit();
	static std::unique_ptr< HUD >	sInstance;
	bool	ShowScoreboard;

	void Render();

	void			SetPlayerHealth( int inHealth )	{ mHealth = inHealth; }
	void			SetTeamBaseHealth(int inHealth) { mTeamBaseHealth = inHealth; }
	void			SetTeam(Team playerTeam) { mTeam = playerTeam; };
	std::string		PadString(std::string toPad, int length);

private:

	HUD();

	void	RenderBandWidth();
	void	RenderRoundTripTime();
	void	RenderScoreBoard();
	void	RenderHealth();
	void	RenderBaseHealth();
	void	RenderText( const string& inStr, const Vector3& origin, const Team& inTeam, TTF_Font* fontToUse );

	Vector3										mBandwidthOrigin;
	Vector3										mRoundTripTimeOrigin;
	Vector3										mScoreBoardOrigin;
	Vector3										mScoreOffset;
	Vector3										mHealthOffset;
	Vector3										mTeamBaseHealthOffset;
	Vector3										mLiberatorOrigin;
	Vector3										mResistanceOrigin;
	SDL_Rect									mViewTransform;

	TTF_Font*									mFont;
	TTF_Font*									mFontDesign;
	TTF_Font*									mFontStrongDesign;
	int											mHealth;
	int											mTeamBaseHealth;

	string										mLiberatorDisplay;
	string										mResistanceDisplay;

	Team										mTeam;
	Vector3										GetTeamColor(Team team);
};

