class SpriteComponent
{
public:

	SpriteComponent(GameObject* inGameObject, bool isTurretSprite);
	SpriteComponent(GameObject* inGameObject) : SpriteComponent(inGameObject, false) {};
	~SpriteComponent();

	virtual void		Draw( const SDL_Rect& inViewTransform );

	void SetTexture(TexturePtr inTexture);

	Vector3	GetOrigin()	const { return mOrigin; }
	void SetOrigin( const Vector3& inOrigin ) { mOrigin = inOrigin; }
			
	Vector3 GetTextureBounds();
	void SetTextureWidth(int inWidth);

	bool GetIsTurretSprite() { return mIsTurretSprite; }


private:

	Vector3											mOrigin;

	TexturePtr										mTexture;

	//don't want circular reference...
	GameObject*										mGameObject;

	bool mIsTurretSprite;
};

typedef shared_ptr< SpriteComponent >	SpriteComponentPtr;