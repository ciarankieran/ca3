/*
Worked on by:		Kieran
Purpose:			Client side of Obstacle class
Notes:
*/
class ObstacleClient : public Obstacle
{
public:
	static	GameObjectPtr	StaticCreate() { return GameObjectPtr(new ObstacleClient()); }

protected:
	ObstacleClient();

private:
	SpriteComponentPtr	mSpriteComponent;
};

