#include "SDL_mixer.h";
/*
Worked on by:		Kieran
Purpose:			To play sounds, implementation of SDL_Mixer
Notes:
*/
class SoundManager
{
public:

	static void StaticInit();
	static std::unique_ptr< SoundManager >	sInstance;

	void PlayMusic();
	void PlayFireSound();
	void PlayExplosionSound();

protected:
	SoundManager();

	Mix_Music* mMusic = NULL;
	
	Mix_Chunk *mEngine = NULL;
	Mix_Chunk *mShoot = NULL;
	Mix_Chunk *mExplosion = NULL;


};