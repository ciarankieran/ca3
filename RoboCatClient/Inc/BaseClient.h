/*
Worked on by:		Kieran
Purpose:			Client side of Base class
Notes:
*/
class BaseClient : public Base
{
public:
	static	GameObjectPtr	StaticCreate() { return GameObjectPtr(new BaseClient() ); }

	virtual void	Read(InputMemoryBitStream& inInputStream) override;

protected:
	BaseClient();
	
	

private:
	SpriteComponentPtr	mSpriteComponent;

	
};

