#pragma once
/*
Worked on by:		Kieran
Purpose:			Client side of Bar class
Notes:				
*/
class BarClient : public Bar
{
public:
	static	GameObjectPtr	StaticCreate() { return GameObjectPtr(new BarClient()); }

	//virtual void	Read(InputMemoryBitStream& inInputStream) override;

protected:
	BarClient();

private:
	SpriteComponentPtr	mSpriteComponent;
};