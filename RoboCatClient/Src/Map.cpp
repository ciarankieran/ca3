#include "RoboCatClientPCH.h"



Map::Map()
{
	mSpriteComponent.reset(new SpriteComponent(this));
	mSpriteComponent->SetTexture(TextureManager::sInstance->GetTexture("map"));
	mSpriteComponent->SetOrigin(Vector3(0, 0, 0));
	SetScale(GetScale() * 1.f);
}

bool Map::HandleCollisionWithCat(RoboCat* inCat)
{
	(void)inCat;
	return false;
}