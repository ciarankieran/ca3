#include <RoboCatClientPCH.h>

std::unique_ptr< HUD >	HUD::sInstance;


HUD::HUD() :
	mScoreBoardOrigin(450.f, 100.f, 0.0f),
	mBandwidthOrigin(50.f, 10.f, 0.0f),
	mLiberatorOrigin(50.f, 120.f, 0.0f),
	mResistanceOrigin(50.f, 0.f, 0.f),
	mRoundTripTimeOrigin(50.f, 10.f, 0.0f),
	mScoreOffset(0.f, 50.f, 0.0f),
	mHealthOffset(1000, 10.f, 0.0f),
	mTeamBaseHealthOffset(1000, 50.f, 0.0f),
	mHealth(0),
	mTeamBaseHealth(50),
	mLiberatorDisplay("Liberators"),
	mResistanceDisplay("Resistance")
{
	TTF_Init();
	mFont = TTF_OpenFont( "../Assets/Carlito-Regular.TTF", 36 );
	mFontDesign = TTF_OpenFont("../Assets/Quantico.TTF", 36);
	mFontStrongDesign = TTF_OpenFont("../Assets/WorldConflict.TTF", 48);
	ShowScoreboard = false;

	if( mFont == nullptr )
	{
		SDL_LogError( SDL_LOG_CATEGORY_ERROR, "Failed to load font." );
	}
}


void HUD::StaticInit()
{
	sInstance.reset( new HUD() );
}

void HUD::Render()
{
	//RenderBandWidth();
	//RenderRoundTripTime();

	if (ShowScoreboard)
	{
		RenderScoreBoard();
	}
}

void HUD::RenderHealth()
{
	if( mHealth > 0 )
	{
		string healthString = StringUtils::Sprintf( "Health %d", mHealth );
		RenderText( healthString, mHealthOffset, mTeam, mFont);
	}
}

void HUD::RenderBaseHealth()
{
	if (mTeamBaseHealth > 0)
	{
		string baseHealthString = StringUtils::Sprintf("BaseHealth %d", mTeamBaseHealth);
		RenderText(baseHealthString, mTeamBaseHealthOffset, mTeam, mFont);
	}
}

void HUD::RenderBandWidth()
{
	string bandwidth = StringUtils::Sprintf( "In %d  Bps, Out %d Bps",
												static_cast< int >( NetworkManagerClient::sInstance->GetBytesReceivedPerSecond().GetValue() ),
												static_cast< int >( NetworkManagerClient::sInstance->GetBytesSentPerSecond().GetValue() ) );
	RenderText( bandwidth, mBandwidthOrigin, mTeam, mFont);
}

void HUD::RenderRoundTripTime()
{
	float rttMS = NetworkManagerClient::sInstance->GetAvgRoundTripTime().GetValue() * 1000.f;

	string roundTripTime = StringUtils::Sprintf( "RTT %d ms", ( int ) rttMS  );
	RenderText( roundTripTime, mRoundTripTimeOrigin, mTeam, mFont);
}

void HUD::RenderScoreBoard()
{
	const vector< ScoreBoardManager::Entry >& entries = ScoreBoardManager::sInstance->GetEntries();
	Vector3 offset = mScoreBoardOrigin;
	RenderText(PadString("Player", 20) + " " + PadString("Kills", 7) +  PadString("Wins", 7), offset, Team::Any, mFontDesign);
	offset.mX += mScoreOffset.mX * 1.5f;
	offset.mY += mScoreOffset.mY * 1.5f;

	mLiberatorOrigin.mY = offset.mY;
	RenderText(mLiberatorDisplay, mLiberatorOrigin, Team::Liberator, mFontStrongDesign);

	for( const ScoreBoardManager::Entry& entry: entries )
	{
		RenderText( entry.GetFormattedNameScore(), offset, entry.GetTeam(), mFontDesign );
		offset.mX += mScoreOffset.mX;
		offset.mY += mScoreOffset.mY;
	}

	if (offset.mY - mScoreOffset.mY != mLiberatorOrigin.mY)
	{
		mResistanceOrigin.mY = offset.mY - mScoreOffset.mY + 20.f;
	}
	else
	{
		mResistanceOrigin.mY = offset.mY + 20.f;
	}

	RenderText(mResistanceDisplay, mResistanceOrigin, Team::Resistance, mFontStrongDesign);
}

void HUD::RenderText( const string& inStr, const Vector3& origin, const Team& inTeam, TTF_Font* fontToUse)
{
	Vector3 inColor = HUD::GetTeamColor(inTeam);

	// Convert the color
	SDL_Color color;
	color.r = static_cast<Uint8>( inColor.mX * 255 );
	color.g = static_cast<Uint8>( inColor.mY * 255 );
	color.b = static_cast<Uint8>( inColor.mZ * 255 );
	color.a = 255;

	if (fontToUse == nullptr)
	{
		fontToUse = mFont;
	}

	// Draw to surface and create a texture
	SDL_Surface* surface = TTF_RenderText_Blended( fontToUse, inStr.c_str(), color );
	SDL_Texture* texture = SDL_CreateTextureFromSurface( GraphicsDriver::sInstance->GetRenderer(), surface );

	// Setup the rect for the texture
	SDL_Rect dstRect;
	dstRect.x = static_cast<int>( origin.mX );
	dstRect.y = static_cast<int>( origin.mY );
	SDL_QueryTexture( texture, nullptr, nullptr, &dstRect.w, &dstRect.h );

	// Draw the texture
	SDL_RenderCopy( GraphicsDriver::sInstance->GetRenderer(), texture, nullptr, &dstRect );

	// Destroy the surface/texture
	SDL_DestroyTexture( texture );
	SDL_FreeSurface( surface );
}

Vector3 HUD::GetTeamColor(Team team)
{
	//this allows us to change all the team colors from one location 
	if (team == Team::Liberator)
	{
		return Vector3(0.2f, 0.2f, 0.2f);
	}
	else if (team == Team::Resistance)
	{
		return Vector3(0.4f, 0.1f, 0.2f);
	}
	else
	{
		return Vector3(0.9f, 0.9f, 0.9f);
	}
}

std::string HUD::PadString(std::string toPad, int length)
{
	while (toPad.length() < length)
	{
		toPad += ' ';
	}

	while(toPad.length() > length)
	{
		toPad.erase(toPad.end());
	}

	return toPad;
}
