
#include <RoboCatClientPCH.h>

bool Client::StaticInit( )
{
	// Create the Client pointer first because it initializes SDL
	Client* client = new Client();

	if( WindowManager::StaticInit() == false )
	{
		return false;
	}
	
	if( GraphicsDriver::StaticInit( WindowManager::sInstance->GetMainWindow() ) == false )
	{
		return false;
	}

	TextureManager::StaticInit();
	RenderManager::StaticInit();
	InputManager::StaticInit();

	HUD::StaticInit();

	SoundManager::StaticInit();

	SDL_Rect viewport = GraphicsDriver::sInstance->GetLogicalViewport();

	GameObjectPtr go = Map::StaticCreate();
	RenderManager::sInstance->SetMapBounds(go->GetSpriteComponentBounds());
	World::sInstance->SetMapTextureBounds(go->GetSpriteComponentBounds());
	World::sInstance->SetViewportScale(Vector3(viewport.w, viewport.h, 0));
	World::sInstance->AddGameObject(go);

	sInstance.reset( client );

	return true;
}

Client::Client()
{
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'RCAT', RoboCatClient::StaticCreate );
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'MOUS', MouseClient::StaticCreate );
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'YARN', YarnClient::StaticCreate );
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'OBST', ObstacleClient::StaticCreate );
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'BASE', BaseClient::StaticCreate );
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'HBAR', BarClient::StaticCreate);
	

	//GameObjectRegistry::sInstance->RegisterCreationFunction('BMAP', Map::StaticCreate);

	string destination = "";
	string name = "";
	string password = "";

	std::fstream fileStream;
	fileStream.open("../Config/ConnectionConfig.txt", std::fstream::in);
	std::getline(fileStream, destination);
	std::getline(fileStream, name);
	std::getline(fileStream, password);
	fileStream.close();

	if (destination.empty())
	{
		ConfigFileErrorLog("Destination");
	}
	else if(name.empty())
	{
		ConfigFileErrorLog("Name");
	}
	else if (password.empty())
	{
		ConfigFileErrorLog("Password");
	}
	else
	{
		SocketAddressPtr serverAddress = SocketAddressFactory::CreateIPv4FromString(destination);

		NetworkManagerClient::StaticInit(*serverAddress, name, password);

		//NetworkManagerClient::sInstance->SetDropPacketChance( 0.6f );
		//NetworkManagerClient::sInstance->SetSimulatedLatency( 0.25f );
		//NetworkManagerClient::sInstance->SetSimulatedLatency( 0.5f );
		//NetworkManagerClient::sInstance->SetSimulatedLatency( 0.1f );
	}
}

void Client::ConfigFileErrorLog(string parameter)
{
	LOG("Parameters not met in ConnectionConfig.txt for %s", parameter);
}



void Client::DoFrame()
{
	InputManager::sInstance->Update();

	Engine::DoFrame();

	NetworkManagerClient::sInstance->ProcessIncomingPackets();

	RenderManager::sInstance->Render();

	NetworkManagerClient::sInstance->SendOutgoingPackets();
}

void Client::HandleEvent( SDL_Event* inEvent )
{
	switch( inEvent->type )
	{
	case SDL_KEYDOWN:
		InputManager::sInstance->HandleInput( EIA_Pressed, inEvent->key.keysym.sym );
		break;
	case SDL_KEYUP:
		InputManager::sInstance->HandleInput( EIA_Released, inEvent->key.keysym.sym );
		break;
	default:
		break;
	}
}

