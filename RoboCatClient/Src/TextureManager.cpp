#include <RoboCatClientPCH.h>


std::unique_ptr< TextureManager >		TextureManager::sInstance;

void TextureManager::StaticInit()
{
	sInstance.reset( new TextureManager() );
}

TextureManager::TextureManager()
{
	CacheTexture( "cat", "../Assets/cat.png" );
	CacheTexture( "mouse", "../Assets/mouse.png" );
	CacheTexture( "yarn", "../Assets/yarn.png" );
	CacheTexture( "base", "../Assets/base.png");
	CacheTexture( "obstacle", "../Assets/obstacle.png");

	CacheTexture("liberatorChassis", "../Assets/LiberatorChassis.png");
	CacheTexture("resistanceChassis", "../Assets/ResistanceChassis.png");
	CacheTexture("liberatorTurret", "../Assets/LiberatorTurret.png");
	CacheTexture("resistanceTurret", "../Assets/ResistanceTurret.png");

	CacheTexture("projectileTankShell", "../Assets/ProjectileTankShell.png");

	CacheTexture("boostBulletSpeed", "../Assets/BoostBulletSpeed.png");
	CacheTexture("boostTankSpeed", "../Assets/BoostTankSpeed.png");
	CacheTexture("boostTankRepair", "../Assets/BoostRepair.png");
	CacheTexture("boostExtraAmmo", "../Assets/BoostExtraAmmo.png");

	CacheTexture("map", "../Assets/map.png");

	CacheTexture("bar", "../Assets/bar.png");
}

TexturePtr	TextureManager::GetTexture( const string& inTextureName )
{
	
	return mNameToTextureMap[ inTextureName ];
}

//SDL_Texture* TextureManager::GetMapTexture()
//{
//	//return SDL
//	SDL_Surface* tempSurface = IMG_Load("../Assets/map.png");
//	SDL_Texture* tex = SDL_CreateTextureFromSurface(GraphicsDriver::sInstance->GetRenderer(), tempSurface);
//	return tex;
//}

bool TextureManager::CacheTexture( string inTextureName, const char* inFileName )
{
	SDL_Texture* texture = IMG_LoadTexture( GraphicsDriver::sInstance->GetRenderer(), inFileName );

	if( texture == nullptr )
	{
		SDL_LogError( SDL_LOG_CATEGORY_ERROR, "Failed to load texture: %s", inFileName );
		return false;
	}

	int w, h;
	SDL_QueryTexture( texture, nullptr, nullptr, &w, &h );

	// Set the blend mode up so we can apply our colors
	SDL_SetTextureBlendMode( texture, SDL_BLENDMODE_BLEND );
	
	TexturePtr newTexture( new Texture( w, h, texture ) );

	mNameToTextureMap[ inTextureName ] = newTexture;

	return true;

}