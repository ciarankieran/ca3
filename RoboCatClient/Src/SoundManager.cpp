#include "RoboCatClientPCH.h"

std::unique_ptr< SoundManager >	SoundManager::sInstance;

void SoundManager::StaticInit()
{
	sInstance.reset(new SoundManager());
	//Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048);
}

SoundManager::SoundManager()
{
	if (SDL_INIT_AUDIO < 0) {
		printf("SDL AUDIO COULDNT INITIALISE");
	}

	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 4096) < 0) {
		printf("SDL_mixer could not initialize! SDL_mixer Error: %s\n", Mix_GetError());
	}

	mMusic = Mix_LoadMUS("../Assets/Sound/mainmusic.wav");
	string a = Mix_GetError();
	mEngine = Mix_LoadWAV("../Assets/Sound/MovementLoop.wav");
	mShoot = Mix_LoadWAV("../Assets/Sound/TankFire.wav");
	mExplosion = Mix_LoadWAV("../Assets/Sound/Explosion.wav");
	PlayMusic();
}

void SoundManager::PlayMusic()
{
	Mix_PlayMusic(mMusic, -1);
}

void SoundManager::PlayFireSound()
{
	Mix_PlayChannel(-1, mShoot, 0);
}

void SoundManager::PlayExplosionSound()
{
	Mix_PlayChannel(-1, mExplosion, 0);
}
