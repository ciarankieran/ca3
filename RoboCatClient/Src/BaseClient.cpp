#include "RoboCatClientPCH.h"

BaseClient::BaseClient()
{
	mSpriteComponent.reset(new SpriteComponent(this));
	mSpriteComponent->SetTexture( TextureManager::sInstance->GetTexture( "base" ) );
}


void BaseClient::Read(InputMemoryBitStream& inInputStream)
{
	bool stateBit;

	uint32_t readState = 0;

	Team teamType;
	int health;

	inInputStream.Read(stateBit);
	if (stateBit)
	{
		Vector3 location;
		inInputStream.Read(location.mX);
		inInputStream.Read(location.mY);
		SetLocation(location);

		float rotation;
		inInputStream.Read(rotation);
		SetRotation(rotation);

		readState |= EBRS_Pose;
		
	}

	inInputStream.Read(stateBit);
	if (stateBit)
	{
		int team;
		inInputStream.Read(team);
		teamType = static_cast<Team>(team);
		SetTeam(teamType);
		

		readState |= EBRS_Team;
	}

	inInputStream.Read(stateBit);
	if (stateBit)
	{
		health = 0;
		inInputStream.Read(health, 4);
		SetBaseHealth(health);

		if (health <= 0)
		{
			NetworkManagerClient::sInstance->SendGameStatePacket(GameState::GS_Finished);
		}

		readState |= EBRS_Health;
	}

	//did we get health? if so, tell the hud!
	if ((readState & EBRS_Health) != 0)
	{
		HUD::sInstance->SetTeamBaseHealth(health);
	}
	
}

