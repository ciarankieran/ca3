#include <RoboCatClientPCH.h>

ObstacleClient::ObstacleClient()
{
	mSpriteComponent.reset(new SpriteComponent(this));
	mSpriteComponent->SetTexture(TextureManager::sInstance->GetTexture("obstacle"));
}
