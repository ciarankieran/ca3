#include <RoboCatClientPCH.h>

std::unique_ptr< WindowManager >	WindowManager::sInstance;

bool WindowManager::StaticInit()
{
	SDL_Window* wnd = SDL_CreateWindow( "Freedom By Force", 200, 200, 1280, 720, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE );
	
	if (wnd == nullptr)
	{
		SDL_LogError( SDL_LOG_CATEGORY_ERROR, "Failed to create window." );
		return false;
	}

 	sInstance.reset( new WindowManager( wnd ) );

	return true;
}


WindowManager::WindowManager( SDL_Window* inMainWindow )
{
	mMainWindow = inMainWindow;	


	//Trying to render background
	//SDL_Surface* surface = IMG_Load("../Assets/map.png");
	//SDL_Texture* texture = SDL_CreateTextureFromSurface(GraphicsDriver::sInstance->GetRenderer(), surface);
	//SDL_FreeSurface(surface);

	//SDL_Rect destination;
	//destination.x = 0; //start x
	//destination.y = 0; //start y
	//destination.w = 200; //width
	//destination.h = 2000; //height

	//SDL_RenderCopy(GraphicsDriver::sInstance->GetRenderer(), texture, NULL, &destination);

}

WindowManager::~WindowManager()
{
	SDL_DestroyWindow( mMainWindow );
}
